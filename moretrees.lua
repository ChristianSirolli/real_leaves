-- Attempting to hook into the mapgen decoration generation
-- Wrapping spawn_Tree might suffice

local deco_ids = {}
--[[
	this is purposefully wrapped in a on mods loaded callback to that it gets the proper ids
	if other mods clear the registered decorations
]]
core.register_on_mods_loaded(function()
	for k, v in pairs(deco_ids) do
        if v.decoration then
            local dec = v.decoration
            if type(v.decoration) == 'table' then
                dec = v.decoration[1]
            end
            if string.find(dec, 'moretrees:') then
                table.insert(deco_ids, core.get_decoration_id(dec))
            end
        end
	end
	core.set_gen_notify("decoration", deco_ids)
end)

core.register_on_generated(function(minp, maxp, blockseed)
    local g = core.get_mapgen_object("gennotify")
    local locations = {}
	for _, id in pairs(deco_ids) do
		local deco_locations = g["decoration#" .. id] or {}
		for _, pos in pairs(deco_locations) do
			locations[#locations+1] = pos
		end
	end

    if #locations == 0 then
        return
    else
        for _, pos in ipairs(locations) do
            -- local timer = core.get_node_timer({x=pos.x, y=pos.y+1, z=pos.z})
            -- timer:start(math.random(2,10))
        end
    end
end)