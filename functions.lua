function bettertrees.assign_uuid(pos1, pos2, nodes)
    local found_nodes = true
    local y = 0
	local uuid = objectuuids.utils.generate_uuid4()
    while found_nodes do
        local objects, num = core.find_nodes_in_area(
            -- { x = pos.x - 1, y = pos.y + y, z = pos.z - 1 },
            pos1, pos2,
            -- { x = pos.x + 1, y = pos.y + y, z = pos.z + 1 },
            nodes)
        if #objects > 0 then
            for i = 1, #objects do
                local meta = core.get_meta(objects[i])
                if meta:contains('bettertrees_uuid') and meta:get('bettertrees_uuid') ~= uuid then
                    uuid = meta:get('bettertrees_uuid')
                else
                    meta:set_string('bettertrees_uuid', uuid)
                end
            end
            y = y + 1;
        else
            found_nodes = false
        end
    end
end